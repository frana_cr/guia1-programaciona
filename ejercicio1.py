#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Tarjeta:
    # Constructor de la funcion
    def __init__(self, nombre, apellido, saldo):
        self.__nombre = " "
        self.__apellido = " "
        self.__saldo = 0

# Se crea el metodo cambio nombre
    def set_nombre(self, nuevo_nombre):
        self.__nombre = nuevo_nombre

# Se crea el metodo que modifique apellido
    def set_apellido(self, nuevo_apellido):
        self.__apellido = nuevo_apellido

# Se crea el metodo para cambiar saldo
    def set_cambio_saldo(self, saldo):
        self.__saldo = saldo

# Se muestra el saldo nuevo luego compra
    def get_saldo_nuevo(self):
        return self._saldo

# Se crea el metodo para obtener el saldo final
    def compra(self, valor_compra):
        nuevo_saldo = (self.__saldo - valor_compra)
        print("Su actual es:", nuevo_saldo)