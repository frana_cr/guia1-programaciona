#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Se crea el constructor
'''el parametro es un entero'''
class Amigo:
    def __init__(self):
        self.dinero = 0

# Se solicita ingresar la cantidad de dinero de cada uno
    def solicitar_dinero(self):
        self.dinero = int(input("Ingrese su saldo actual"))

# Se muestra la cantidad de saldo
    def get_cantidad_saldo(self):
        return self.dinero
