#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Se crea la clase Encriptar donde se obtendran los digitos del numero
class Encriptar:
    def __init__(self, numero):
        self.__numero = numero

# Se crea el metodo para poder modificiar al numero
    def set_numero(self, numero):
        self.__numero = numero

# Se crea el metodo para obtener al numero
    def get_numero(self):
        return self.__numero

# Con este metodo se pueden obtener las unidades del numero
    def unidades_numero(self):
        # ~ a = b = c = d = 0
        if (self.__numero >= 1000):
            if (self.__numero <= 9999):
                # Se obtienen las cifras de cada unidad del numero
                a = self.__numero / 1000
                b = (self.__numero % 1000) / 100
                c = (self.__numero % 1000) % 100 / 10
                d = ((self.__numero % 1000) % 100) % 10 / 1
        print("Los digitos del numero son", int(a), int(b), int(c), int(d))
        return int(a), int(b), int(c), int(d)