#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Se crea el constructor
class Perro:
    def __init__(self, color, raza, edad, genero, kilos):
        self.__color = " "
        self.__raza = " "
        self.__edad = 0
        self.__genero = 0
        self.__kilos = 0

# Se crea el metodo que imprime los atributos fisicos
    def atributos_fisicos(self, color, raza, edad, genero, kilos):
        print("El color del perro es:", color)
        print("La raza del perro es:", raza)
        print("La edad del perro es:", edad)
        print("El genero del perro es:", genero)
        print("El peso del perro es:", kilos)

# Se crea el metodo que permite el movimiento
    def movimiento_perro(self, movimiento):
        if(movimiento == 1):
            self.movimiento = True
            print("El perro esta jugando")
        else:
            self.movimiento = False
            print("El perro no esta jugando")

# Se crea el metodo que permite el sonido del perro
    def sonido_perro(self, sonido):
        if(sonido == 1):
            self.sonido = True
            print("El perro ladra")
            print("Guauuu")
        else:
            self.sonido = False
            print("No ladra")

# Se crea el metodo que permite al perro alimentarse
    def comer_perro(self, comer):
        if(comer == 1):
            self.comer = True
            print("El perro esta comiendo")
        else:
            self.comer = False
            print("El perro no come")

# Se crea el metodo de la disputa si es macho
    def pelea_disputa(self, genero):
        if(genero == 1):
            self.genero = True
            print("Los perros se pelean entre si")
        else:
            self.genero = False
            print("Los perros no se pelean")