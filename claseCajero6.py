#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Se crea el objeto con su clase respectiva
class Cuenta:
    def __init__(self, nombre_cliente, numero_cuenta, saldo, tipo_interes):
        self.__nombre_cliente = nombre_cliente
        self.__numero_cuenta = numero_cuenta
        self.__saldo = saldo
        self.__tipo_interes = tipo_interes

    # Se crea el metodo para poder obtener el numero de cuenta
    def get_numero_cuenta(self):
        return self.numero_cuenta

    # Se crea el metodo para modificar el numero de cuenta
    def set_numero_cuenta(self, numero_cuenta):
        self.numero_cuenta = numero_cuenta

    # Se crea el metodo para obtener el numero de cuenta
    def get_nombre_cliente(self):
        return self.nombre_cliente

    # Se crea el metodo para cambiar el nombre de cliente
    def set_nombre_cliente(self, nombre_cliente):
        self.nombre_cliente = nombre_cliente

    # Se crea el metodo para ver el saldo
    def get_saldo(self):
        return self.saldo

    # Se crea el metodo cambiar el saldo
    def set_saldo(self, saldo):
        self.saldo = saldo

    # Se crea el metodo para obtener el tipo de interes
    def get_tipo_interes(self):
        return self.tipo_interes

    # Se crea el metodo para cambiar el tipo de interes
    def set_tipo_interes(self, tipo_interes):
        self.tipo_interes = tipo_interes

    # Se crea el metodo para ingresar el dinero del deposito
    def ingreso_dinero(self, saldo):
        ingreso = int(input("Ingrese el deposito"))
        if(ingreso < 0):
            # Si es falso no se realiza el deposito
            self.ingreso = False
            print("Cifra Incorrectaintenta nuevamente")
            ingreso_dinero(self, saldo)
        else:
            # Si es verdad se lleva a cabo el deposito
            self.ingreso = True
            saldo_nuevo = (saldo + ingreso)
            print("Su saldo ahora es", saldo_nuevo)
            self.set_saldo(saldo_nuevo)

    # Se crea el metodo para el giro de dinero
    def giro_dinero(self, saldo):
        giro = int(input("Ingrese la cantidad a girar"))
        if(giro < 0):
            # Si es falso no se realiza el giro
            self.giro = False
            print("La cifra de giro es Incorrecta")
            giro_dinero(self, saldo)
        else:
            # Si es verdad se lleva a cabo el giro
            self.giro = True
            saldo_nuevo1 = (saldo - giro)
            print("Su saldo ahora es", saldo_nuevo1)
            self.set_saldo(saldo_nuevo1)

        # Se crea el metodo para obtener lo que cada amigo cancela
    def get_pago_cada_amigo(self):
        return self.set_saldo(saldo_nuevo1)

        # Se crea el metodo para que suceda la transferencia
    def metodo_transferencia(self, saldo_nuevo1):
        if(saldo_nuevo1 < 0):
            print("No se tranferir")
        else:
            print("Se puede hacer la transferencia")