#!/usr/bin/env python
# -*- coding: utf-8 -*-
from claseCajero6 import Cuenta

# Se crea el menu para las interacciones con el usuario
if __name__ == '__main__':
    # Se crea la interaccion con el usuario
    print("Bienvenido al cajero")
    nombre_cliente = input("Ingrese el nombre del cliente")
    numero_cuenta = int(input("Ingrese el numero de cuenta"))
    saldo = int(input("Ingrese saldo de la cuenta"))
    tipo_interes = float(input("Ingrese la tasa de interes"))

    # Se llama al objeto de la funcion
    cuenta_bancaria = Cuenta(nombre_cliente, numero_cuenta, saldo, tipo_interes)
    # Se llaman a los metodos
    cuenta_bancaria.ingreso_dinero(saldo)
    cuenta_bancaria.giro_dinero(saldo)
    cuenta_bancaria.metodo_transferencia(saldo)
