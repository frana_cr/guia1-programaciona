#!/usr/bin/env python
# -*- coding: utf-8 -*-


class CalcularCuenta:
    # Constructor de la funcion
    def __init__(self, cantidad_amigos, valor_cuenta):
        self.__valor_cuenta = valor_cuenta
        self.__cantidad_amigos = cantidad_amigos
        self.__pago_individual1 = 0

# Se crea el metodo para modificar valor cuenta
    def set_valor_cuenta(self, valor_cuenta):
        self.valor_cuenta = valor_cuenta

# Se crea el metodo para modificar la cantidad de amigos
    def set_cantidad_amigos(self, cantidad_amigos):
        self.cantidad_amigos = cantidad_amigos

# Se crea el metodo para obtener el saldo final
    def consumo(self, valor_cuenta):
        cancelar = (self.__valor_cuenta * 0.19)
        propina = (cancelar * 0.10)
        monto_final = valor_cuenta + (cancelar + propina)
        print("La cuenta en total es:", monto_final)
        self.set_valor_cuenta(monto_final)

# Se crea el metodo para mostrar el saldo final
    def get_nuevo_valor(self):
        return self.valor_cuenta

# Se crea el metodo para obtener el valor individual a cancelar
    def pago_individual(self, monto_final, cantidad_amigos):
        self.pago_individual1 = (monto_final / cantidad_amigos)
        print("Cada uno debe pagar:", self.pago_individual1)

# Se crea el metodo que retorna el valor que cada uno cancela
    def get_pago_cada_amigo(self):
        return self.pago_individual1