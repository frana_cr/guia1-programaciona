#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se llama al constructor del otro archivo
from perro5 import Perro

if __name__ == '__main__':
# Se produce interaccion con usuario
    color = input("Ingrese el color del perro")
    raza = input("Ingrese la raza del perro")
    edad = int(input("Ingrese la edad del perro"))
    genero = int(input("Ingrese 1: Macho, Ingrese 2: Hembra"))
    kilos = int(input("Ingrese los kilos del perro"))
    movimiento = int(input("Ingrese 1: movimiento, 2:No Movimiento"))
    sonido = int(input("Ingrese 1: perro ladra, Ingrese otro valor si no"))
    comer = int(input("Ingrese 1:comer, 2:No comer"))

    # Se nombra al constructor
    caracteristicas_fisicas = Perro(color, raza, edad, genero, kilos)

    # Los metodos son ingresados para implementarlos
    caracteristicas_fisicas.atributos_fisicos(color, raza, edad, genero, kilos)
    caracteristicas_fisicas.movimiento_perro(movimiento)
    caracteristicas_fisicas.sonido_perro(sonido)
    caracteristicas_fisicas.comer_perro(comer)
    caracteristicas_fisicas.pelea_disputa(genero)