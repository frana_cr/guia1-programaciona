#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se llama a la funcion encriptar desde su archivo
from encriptar3 import Encriptar


# Se crea la funcion sumatoria donde al numero se le suman los otros siguientes
def sumatoria(numero_pedir):
    n = 0
    suma = 0
    for i in range(int(numero_pedir)):
        # De este modo se efectuara la suma
        suma = suma + 1 + i
    if(suma >= 10):
        n = terminos(suma)
        return int(n)
    else:
        return int(suma)


# Se crea la funcion donde a cada termino si es mayor a 10 se obtiene el resto
def terminos(numero_x):
    n = (numero_x % 10 / 1)
    return int(n)


# A cada numero se le sumara 7 luego de realizado estas etapas
def sumar_7(numero_y):
    n = 0
    suma_numeros = 0
    suma_numeros = numero_y + 7
    if(suma_numeros >= 10):
        n = terminos(suma_numeros)
        return int(n)
    else:
        return int(suma_numeros)


# Se crea el main de la funcion
if __name__ == '__main__':
    # Se solicita el numero al usuario
    numero = int(input("Ingrese un numeros de 4 cifras:"))
    number = Encriptar(numero)
    # Al numero se le integra la clase
    a, b, c, d = number.unidades_numero()
    a1 = sumatoria(a)
    b1 = sumatoria(b)
    c1 = sumatoria(c)
    d1 = sumatoria(d)

    # Se imprime el nuevo numero del codigo
    print("El numero es: ", sumar_7(c1), sumar_7(d1), sumar_7(a1), sumar_7(b1))