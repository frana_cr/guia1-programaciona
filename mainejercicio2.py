# -*- coding: utf-8 -*-
from libro_ejercicio2 import Libro
from biblioteca_ejercicio2 import Biblioteca

if __name__ == '__main__':
    print("Bienvenido a la Biblioteca")
    # Se comienza con la interaccion del usuario
    titulo_libro = input("Ingrese el nombre del libro")
    autor = input("Ingrese el autor del libro")
    numero_ejemplares = int(input("Ingrese numero de ejemplares"))
    libros_prestados = int(input("Ingrese numero de ejemplares prestados"))
    # Se importan datos
    libros_debiblioteca = Libro(titulo_libro, autor, numero_ejemplares)
    datos_biblioteca = Biblioteca(numero_ejemplares, libros_prestados)

    # Se ingresan los metodos del constructor libros
    libros_debiblioteca.set_libro(titulo_libro)
    libros_debiblioteca.set_autor(autor)
    libros_debiblioteca.libros_biblioteca(titulo_libro, autor)

    # Se ingresan los metodos del constructor biblioteca
    datos_biblioteca.set_numero_ejemplares(numero_ejemplares)
    datos_biblioteca.set_nuevos_libros_prestados(libros_prestados)
    datos_biblioteca.libros_biblioteca(numero_ejemplares, libros_prestados)