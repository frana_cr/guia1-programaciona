#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se llaman a las clases de sus archivos
from calcularcuenta4 import CalcularCuenta
from amigo4 import Amigo

# Se genera el main
if __name__ == '__main__':
    # Se interactua con el usuario
    print("Bienvenido al Restaurant")
    cantidad_amigos = int(input("Ingrese el numero de consumidores"))
    valor_cuenta = int(input("Ingrese el valor de la cuenta"))

    # Se ejecutan los atributos
    consumo_amigos = CalcularCuenta(cantidad_amigos, valor_cuenta)
    consumo_amigos.consumo(valor_cuenta)
    consumo_amigos.pago_individual(consumo_amigos.get_nuevo_valor(), cantidad_amigos)
    # Se genera un for para comprobar que los amigos cancelan
    for i in range(cantidad_amigos):
        cantidad_dinero = Amigo()
        cantidad_dinero.solicitar_dinero()
        if (cantidad_dinero.get_cantidad_saldo() < consumo_amigos.get_pago_cada_amigo()):
            print("Tienes dinero insuficiente amigo cancela")
        else:
            print("Tienes suficiente dinero")