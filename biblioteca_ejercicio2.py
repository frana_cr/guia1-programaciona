#!/usr/bin/env python
# -*- coding: utf-8 -*-
# En esta clase estan los datos del libro en biblioteca


class Biblioteca:
    # Constructor
    # Parametros: numero ejemplares, libros prestados (int)
    def __init__(self, numero_ejemplares, libros_prestados):
        self.__numero_ejemplares = 0
        self.__libros_prestados = 0

# En este metodo se modific el numero de ejemplares'''
    def set_numero_ejemplares(self, numero_ejemplares_nuevos):
        self.__numero_ejemplares = numero_ejemplares_nuevos

# En este metodo se modifica el numero de libros prestados
    def set_nuevos_libros_prestados(self, libros_prestados_actual):
        self.__libros_prestados = libros_prestados_actual

# En este metodo se muestra el numero de ejemplares'''
    def get_numero_ejemplares(self):
        return self._numero_ejemplares

# En este metodo se muestra el numero de libros prestados
    def get_cantidad_ejemplares(self):
        return self._libros_prestados

# En este metodo se imprime los valores numericos del libro
    def libros_biblioteca(self, numero_ejemplares, libros_prestados):
        print("El numero de ejemplares son:", numero_ejemplares)
        print("El numero de ejemplares prestados son:", libros_prestados)